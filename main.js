// Для работы с input не рекомендуется использовать события клавиатуры,
//     потому что существуют и другие методы ввести информацию в input,
//     например с помощью мышки, вставить из буфера обмена.




document.addEventListener('keydown', checkKeyPress, false);

const buttonBackground = document.getElementById('button-wrap').querySelectorAll('.btn');

function checkKeyPress(key) {
    if (key.key == 'Enter') {
        buttonBackground[0].style.background = 'blue';
        buttonBackground[1].style.background = 'black';
        buttonBackground[2].style.background = 'black';
        buttonBackground[3].style.background = 'black';
        buttonBackground[4].style.background = 'black';
        buttonBackground[5].style.background = 'black';
        buttonBackground[6].style.background = 'black';
    } else if (key.key == 's') {
        buttonBackground[1].style.background = 'blue';
        buttonBackground[0].style.background = 'black';
        buttonBackground[2].style.background = 'black';
        buttonBackground[3].style.background = 'black';
        buttonBackground[4].style.background = 'black';
        buttonBackground[5].style.background = 'black';
        buttonBackground[6].style.background = 'black';
    } else if (key.key == 'e') {
        buttonBackground[2].style.background = 'blue';
        buttonBackground[1].style.background = 'black';
        buttonBackground[0].style.background = 'black';
        buttonBackground[3].style.background = 'black';
        buttonBackground[4].style.background = 'black';
        buttonBackground[5].style.background = 'black';
        buttonBackground[6].style.background = 'black';
    } else if (key.key == 'o') {
        buttonBackground[3].style.background = 'blue';
        buttonBackground[1].style.background = 'black';
        buttonBackground[2].style.background = 'black';
        buttonBackground[0].style.background = 'black';
        buttonBackground[4].style.background = 'black';
        buttonBackground[5].style.background = 'black';
        buttonBackground[6].style.background = 'black';
    } else if (key.key == 'n') {
        buttonBackground[4].style.background = 'blue';
        buttonBackground[1].style.background = 'black';
        buttonBackground[2].style.background = 'black';
        buttonBackground[0].style.background = 'black';
        buttonBackground[3].style.background = 'black';
        buttonBackground[5].style.background = 'black';
        buttonBackground[6].style.background = 'black';
    } else if (key.key == 'l') {
        buttonBackground[5].style.background = 'blue';
        buttonBackground[1].style.background = 'black';
        buttonBackground[2].style.background = 'black';
        buttonBackground[0].style.background = 'black';
        buttonBackground[4].style.background = 'black';
        buttonBackground[3].style.background = 'black';
        buttonBackground[6].style.background = 'black';
    } else if (key.key == 'z') {
        buttonBackground[6].style.background = 'blue';
        buttonBackground[1].style.background = 'black';
        buttonBackground[2].style.background = 'black';
        buttonBackground[0].style.background = 'black';
        buttonBackground[4].style.background = 'black';
        buttonBackground[5].style.background = 'black';
        buttonBackground[3].style.background = 'black';
    }
}
